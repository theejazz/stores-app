package dvb.bsc.stores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dvb.bsc.stores.model.SortingOption;
import dvb.bsc.stores.model.Store;
import dvb.bsc.stores.model.StoreManager;
import dvb.bsc.stores.model.StoreType;

@RestController
@RequestMapping("/")
class Controller {

    @Autowired
    private StoreManager storeManager;

    /**
     * A simple landing page. Used for smoke tests.
     */
    @GetMapping
    public String hello() {
        return "Hello, world!";
    }

    /**
     * Find up to `size` stores stored within the system.
     */
    @GetMapping("/search")
    public ResponseEntity<List<Store>> search(
                                        @RequestParam(defaultValue = "20") int size, 
                                        @RequestParam(required = false) String sort) {
        List<Store> stores = null;
        if (sort != null) {
            try {
                SortingOption sOption = SortingOption.valueOf(sort.toUpperCase());
                stores = storeManager.getSortedStores(sOption);
            } catch (IllegalArgumentException iae) {
                return ResponseEntity.badRequest().body(null);
            }
        } else {
            stores = storeManager.getStores();
        }

        return ResponseEntity.ok(
                stores.subList(0, Math.min(size, storeManager.getStores().size())));
    }

    /**
     * Find stores whose name partially matches `query`
     */
    @GetMapping("/search/name/{query}")
    public ResponseEntity<List<Store>> searchByName(@PathVariable String query) {
        boolean isMatch = true;
        if (query.charAt(0) == '-') {
            isMatch = false;
            query = query.substring(1);
        }
        return query.strip().isEmpty() 
            ? ResponseEntity.badRequest().body(null) 
            : ResponseEntity.ok(storeManager.getStoresByName(query, isMatch));
    }

    /**
     * Find stores of a specific type
     */
    @GetMapping("/search/type/{type}")
    public ResponseEntity<List<Store>> searchByType(@PathVariable String type) {
        try {
            StoreType sType = StoreType.valueOf(type.toUpperCase());
            return ResponseEntity.ok(storeManager.getStoresByType(sType));
        } catch (IllegalArgumentException iae) {
            return ResponseEntity.badRequest().body(null);
        }
    }

    // search/from/x/y/within/radius
    @GetMapping("/search/from/{x}/{y}/within/{radius}")
    public ResponseEntity<List<Store>> searchWithinRadius(@PathVariable int x,
                                                            @PathVariable int y,
                                                            @PathVariable int radius) {
        return ResponseEntity.ok(storeManager.getStoresWithinRadius(x, y, radius));
    }


    
}