package dvb.bsc.stores.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

@Component
public class StoreManager {

    public static final int N_STORES = 300;

    public static final int MIN_COORD = -2000;
    public static final int MAX_COORD = 2000;

    public static final int MIN_NAME_LENGTH = 2;
    public static final int MAX_NAME_LENGTH = 7;

    private List<Store> stores;

    public StoreManager() {
        stores = new ArrayList<>();
        generateStores();
    }

    private void generateStores() {
        for (int idx = 0; idx != N_STORES; ++idx) {
            stores.add(new Store(
                randomCoord(),
                randomCoord(),
                StoreType.values()[randomInt(0, StoreType.values().length)],
                "STORE " + randomLetters(randomInt(MIN_NAME_LENGTH, 
                                                    MAX_NAME_LENGTH + 1))));
        }
    }

    public static int randomCoord() {
        return randomInt(MIN_COORD, MAX_COORD + 1);
    }

    public static String randomLetters(int nLetters) {
        String word = "";
        for (int idx = 0; idx != nLetters; ++idx) {
            word += (char) ('a' + randomInt(0, 26));
        }
        return word;
    }

    public static int randomInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }

    public List<Store> getStores() {
        return stores;
    }

    public List<Store> getSortedStores(SortingOption sort) {
        switch (sort) {
            case ALPHABETICAL:
                return stores
                        .stream()
                        .sorted(Comparator.comparing(Store::getName))
                        .collect(Collectors.toList());
            default:
                throw new UnsupportedOperationException();
        }
    }

    /**
     * Retrieve all stores where the name does (not) contain match the substring
     */
    public List<Store> getStoresByName(String subString, boolean isMatch) {
        return stores
            .stream()
            .filter(store -> !isMatch ^ store.getName().contains(subString))
            .collect(Collectors.toList());
    }

    /**
     * Retrieve all stores of a certain type
     */
    public List<Store> getStoresByType(StoreType type) {
        return stores
            .stream()
            .filter(store -> store.getStoreType().equals(type))
            .collect(Collectors.toList());
    }

    /**
     * Retrieve all stores within the circle with center (`x`, `y`) and radius `r`
     */
    public List<Store> getStoresWithinRadius(int x, int y, int r) {
        return stores
            .stream()
            .filter(store -> store.isWithinCircle(x, y, r))
            .collect(Collectors.toList());
    }
}
