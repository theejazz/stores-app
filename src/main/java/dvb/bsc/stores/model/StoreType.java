package dvb.bsc.stores.model;

/** Every `Store` has exactly 1 `StoreType`. No more, no less */
public enum StoreType {
    GROCERY,
    PET,
    PHARMACY,
    HARDWARE
}
