package dvb.bsc.stores.model;

import java.util.UUID;

public class Store {
    int xCoord;
    int yCoord;

    StoreType storeType;

    String id;
    String name;

    public Store(int xCoord, int yCoord, StoreType storeType, String name) {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
        this.storeType = storeType;
        this.name = name;

        id = UUID.randomUUID().toString();
    }

    public Store() {
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Store)) {
            return false;
        }

        Store store = (Store) obj;
        return store.id.equals(id);
    }

    @Override
    public int hashCode() {
        return 31 * 17 + id.hashCode();
    }

    public void setXCoord(int xCoord) {
        this.xCoord = xCoord;
    }

    public void setYCoord(int yCoord) {
        this.yCoord = yCoord;
    }

    public void setStoreType(StoreType storeType) {
        this.storeType = storeType;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getXCoord() {
        return xCoord;
    }

    public int getYCoord() {
        return yCoord;
    }

    public StoreType getStoreType() {
        return storeType;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return id + " " + name + " " + storeType + " " + xCoord + " " + yCoord;
    }

    /**
     * Check if this store is located within the circle with center (`x`, `y`)
     * and radius `r`. This is defined by the inequality:
     *   (x_store - x_circle)^2 + (y_store - y_circle)^2 < radius^2
     */
    public boolean isWithinCircle(int x, int y, int r) {
        int xDistance = xCoord - x;
        xDistance *= xDistance;

        int yDistance = yCoord - y;
        yDistance *= yDistance;

        r *= r;
        return xDistance + yDistance < r;
    }
}
