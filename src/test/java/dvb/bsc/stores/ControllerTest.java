package dvb.bsc.stores;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import dvb.bsc.stores.model.SortingOption;
import dvb.bsc.stores.model.Store;
import dvb.bsc.stores.model.StoreManager;
import dvb.bsc.stores.model.StoreType;
import dvb.bsc.stores.set_theory.SetTheory;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ControllerTest {

    @Autowired
    private Controller controller;

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    private static final String LOCALHOST = "http://localhost:";

    /**
     * Smoke test: Ensure autowired components are configured before testing
     */
    @Test
    public void testContextLoads() {
        assertNotNull(controller);
        assertNotNull(restTemplate);
    }

    /**
     * Smoke test: Ensure the app is live
     * A GET request towards the homepage should result in the String "Hello, world!"
     */
    @Test
    public void testHomepageIsLive() {
        assert("Hello, world!".equals(restTemplate.getForObject(
            LOCALHOST + port,
            String.class)));
    }

    /**
     * Helper method to abstract away GET requests to the search API
     */
    public List<Store> searchStores(String queryParams) {
        return Arrays.asList(restTemplate.getForObject(
            LOCALHOST + port + "/search/" + queryParams, 
            Store[].class));
    }
    
    /** 
     * Test equality for specifying default args
     * E.g. /search and /search?size=20 should produce equal outputs
     * granted that the default value of the size param is 20
     */
    @Test
    public void testSpecifyingDefaultArgumentProducesEqualList() {
        assert(SetTheory.areEqual(
            searchStores(""), 
            searchStores("?size=20")));
    }

    /**
     * Test equivalence for specifying a sorting option
     * E.g. /search and /search/filter/alphabetical should produce equivalent
     * outputs (granted that the `size` param is sufficiently large)
     */
    @Test
    public void testSortingResultsProducesEquivalentList() {
        List<Store> allStores = searchStores("?size=" + StoreManager.N_STORES);
        for (SortingOption sort : SortingOption.values()) {
            assert(SetTheory.areEquivalent(
                allStores,
                searchStores("?size=" + StoreManager.N_STORES 
                                + "&sort=" + sort.toString())));
        }
    }

    /**
     * Test subset for text searches.
     * E.g. /search/name/a should be a superset of /search/name/aa
     * 
     * Example data: {
     *      Store("Store a"),
     *      Store("Store aa"),
     *      Store("Store b")
     * }
     * 
     * The former request should match the first two stores, whereas the latter should match only the second.
     */
    @Test
    public void testMoreSpecificSearchQueryProducesSubset() {
        for (char ch1 = 'a'; ch1 <= 'z'; ++ch1) {
            for (char ch2 = 'a'; ch2 <= 'z'; ++ch2) {
                assert(SetTheory.isSubsetOf(
                    searchStores("name/" + ch1 + ch2), 
                    searchStores("name/" + ch1)));
            }
        }
    }

    /**
     * Test disjoint for exclusionary searches
     * E.g. /search/name/a and /search/name/-a should be disjoint
     */
    @Test
    public void testExclusionarySearchProducesDisjointList() {
        for (char ch = 'a'; ch <= 'z'; ++ch) {
            assert(SetTheory.areDisjoint(
                searchStores("name/" + ch), 
                searchStores("name/-" + ch)));
        }
    }

    /** 
     * Test disjoint for store types
     * E.g. /search/type/hardware and /search/type/pet contain none of the same stores
     * 
     * This metamorphic relation holds only while stores cannot belong to multiple StoreTypes.
     * I.e. 1-to-N StoreType-to-Store relationships only.
     */
    @Test
    public void testDifferent1ToNFilterOptionProducesDisjointList() {
        for (StoreType type1 : StoreType.values()) {
            for (StoreType type2 : StoreType.values()) {
                if (type1.equals(type2)) {
                    continue;
                }

                assert(SetTheory.areDisjoint(
                    searchStores("type/" + type1.toString()), 
                    searchStores("type/" + type2.toString())));
            }
        }
    }

    /**
     * Test equivalence for store types. The union of all stores retrieved when
     * filtering by storeType should contain all stores
     * 
     * This metamorphic relation holds only while all stores are assigned at least
     * one storeType
     */
    @Test
    public void testUnionOfFiltersProducesEquivalentList() {
        List<Store> allStores = searchStores("?size=" + StoreManager.N_STORES);
        List<Store> unionOfFilters = new ArrayList<>();
        for (StoreType sType : StoreType.values()) {
            unionOfFilters.addAll(searchStores("type/" + sType.toString()));
        }

        assert(SetTheory.areEquivalent(allStores, unionOfFilters));
    }

    /**
     * Test subset for areas that enclose eachother
     * E.g. /search/from/0/0/within/1000 should be a superset of /search/from/0/0/within/10
     * 
     * This is tested by picking a random, large area, and repeatedly making it smaller.
     * Everytime the radius decreases, the subset relation is evaluated
     * For example, starting with a radius of 1726, the following checks are performed:
     *      /from/x/y/within/1726 is a superset of /from/x/y/within/1626
     *      /from/x/y/within/1626 is a superset of /from/x/y/within/1526
     *      /from/x/y/within/1526 is a superset of /from/x/y/within/1426
     *      ...
     *      /from/x/y/within/26   is a superset of /from/x/y/within/-74
     *      [end]
     */
    @Test
    public void testEnclosedAreaProducesSubset() {
        int xCoord = StoreManager.randomCoord();
        int yCoord = StoreManager.randomCoord();
        int radius = StoreManager.MAX_COORD - StoreManager.MIN_COORD;

        List<Store> currentStores = searchStores(
            "from/" + xCoord + "/" + yCoord + "/within/" + radius);
        List<Store> enclosedStores = null;

        while (radius > 0) {
            radius -= 100;
            enclosedStores = searchStores(
                "from/" + xCoord + "/" + yCoord + "/within/" + radius);

            assert(SetTheory.isSubsetOf(enclosedStores, currentStores));

            currentStores = enclosedStores;
        }
    }

    /**
     * Test disjointness for areas that do not overlap
     * E.g. /search/from/-50/-50/within/10 should be disjoint from /search/from/50/50/within/10
     * 
     * This is tested by generating a random point, mirroring it across both axes, and generating
     * radii such that neither circles overlap
     */
    @Test
    public void testSeparateAreaProducesDisjointList() {
        int xCoord = StoreManager.randomCoord();
        int yCoord = StoreManager.randomCoord();

        // Casting a double to int will always round down, thus making the radius safe
        int radius = (int) Math.sqrt(xCoord * xCoord + yCoord * yCoord);
        assert(SetTheory.areDisjoint(
            searchStores("from/" + xCoord + "/" + yCoord + "/within/" + radius),
            searchStores("from/" + -xCoord + "/" + -yCoord + "/within/" + radius)));
    }
}
