package dvb.bsc.stores.set_theory;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class SetTheory {
    
    /**
     * l1 is a subset of l2 if l2 contains all items in l1
     */
    public static <T> boolean isSubsetOf(List<T> l1, List<T> l2) {
        return l2.containsAll(l1);
    }

    /**
     * Two lists are equal if they share the same items in the same order
     */
    public static <T> boolean areEqual(List<T> l1, List<T> l2) {
        return l1.equals(l2);
    }

    /**
     * Two lists are disjoint if they do not share any items
     */
    public static <T> boolean areDisjoint(List<T> l1, List<T> l2) {
        return Collections.disjoint(l1, l2);
    }

    /**
     * Two lists are equivalent if they contain the same items
     * The order in which they appear is not of importance
     * 
     * Assumes either: 
     *  (1) neither list contains duplicate elements
     *  (2) duplicate elements may be ignored
     */
    public static <T> boolean areEquivalent(List<T> l1, List<T> l2) {
        return new HashSet<>(l1).equals(new HashSet<>(l2));
    }

}
