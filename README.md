The stores application is small queryable web service built as part of my bachelor's thesis.

Run with `mvn spring-boot:run`.

Test with `mvn test`.

Mutation test with `mvn test-compile org.pitest:pitest-maven:mutationCoverage`.

The default mutation profile is DEFAULTS. This can be changed to MATH or ALL by editing the `pom.xml` file.
